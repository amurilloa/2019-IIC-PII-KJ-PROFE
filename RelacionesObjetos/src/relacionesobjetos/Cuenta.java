/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesobjetos;

import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Cuenta {

    private long numero;
    private float saldo;
    private float interesAnual;
    private Cliente titular;
    private LinkedList<Movimiento> movimientos;

    public Cuenta() {
        movimientos = new LinkedList<>();
    }

    public Cuenta(long numero, float saldo, float interesAnual) {
        this.numero = numero;
        this.saldo = saldo;
        this.interesAnual = interesAnual;
        movimientos = new LinkedList<>();
    }

    public Cuenta(long numero, float saldo, float interesAnual, Cliente titular) {
        this.numero = numero;
        this.saldo = saldo;
        this.interesAnual = interesAnual;
        this.titular = titular;
    }

    public void reintegro(int cantidad) {
        if (cantidad <= 0) {
            return;
        }
        movimientos.add(new Movimiento(LocalDate.now().toString(), 'R', cantidad, saldo -= cantidad));
    }

    public void ingreso(int cantidad) {
        if (cantidad <= 0) {
            return;
        }
        movimientos.add(new Movimiento(LocalDate.now().toString(), 'I', cantidad, saldo += cantidad));
    }

    public String movimientos() {
        StringBuilder mov = new StringBuilder();
        for (Movimiento movimiento : movimientos) {
            mov.append("\t- ").append(movimiento).append("\n");
        }
        return mov.toString();
    }

    public long getNumero() {
        return numero;
    }

    public float leerSaldo() {
        return saldo;
    }

    public float getInteresAnual() {
        return interesAnual;
    }

    public Cliente getTitular() {
        return titular;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public void setInteresAnual(float interesAnual) {
        this.interesAnual = interesAnual;
    }

    public void setTitular(Cliente titular) {
        this.titular = titular;
    }

    @Override
    public String toString() {
        String info = String.format("%d - %.2f(%.2f) | %s", numero, saldo,
                interesAnual, titular == null ? "Sin Cliente" : titular);
        return info;
    }

}
