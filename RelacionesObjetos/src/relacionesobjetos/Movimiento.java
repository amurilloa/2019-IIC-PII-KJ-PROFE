/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesobjetos;

import java.util.Formatter;

/**
 *
 * @author Procopio
 */
public class Movimiento {

    private String fecha;
    private char tipo;
    private float importe;
    private float saldo;

    public Movimiento(String fecha, char tipo, float importe, float saldo) {
        this.fecha = fecha;
        this.tipo = tipo;
        this.importe = importe;
        this.saldo = saldo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        String res = String.format("%s %s %9.0f %9.2f", fecha, tipo, importe, saldo);
        return res;
    }        

}
