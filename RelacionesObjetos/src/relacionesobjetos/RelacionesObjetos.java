/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesobjetos;

/**
 *
 * @author allanmual
 */
public class RelacionesObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //Relación de Asociación
        Cliente c = new Cliente("206470762", "Allan", "Murillo Alfaro", "Chachagua");
        System.out.println(c);

        Cuenta cc = new Cuenta(123123l, 5000.0f, 0.01f);
        cc.setTitular(c);

        System.out.println(cc);

        c.setDireccion("Fortuna");
        cc.ingreso(100000);
        cc.ingreso(10000);
        cc.ingreso(5000);
        cc.reintegro(30000);
        cc.ingreso(30000);
        cc.reintegro(30000);
        cc.reintegro(30000);
        System.out.println(cc);
        System.out.println(cc.movimientos());

    }

}
