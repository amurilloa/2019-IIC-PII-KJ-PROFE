/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrama;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Motor m1 = new Motor();
        Coche c1 = new Coche(m1);
        m1.activa();
        m1.setRpm(2000);
        
        Persona p1 = new Persona("Allan Murillo");
        Persona p2 = new Persona("Luis Murillo");
        p1.setCoche(c1);
        c1.setConductor(p2);
        
        System.out.println(p1);
            
    }
}
