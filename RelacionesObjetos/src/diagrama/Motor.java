/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrama;

/**
 *
 * @author Allan Murillo
 */
public class Motor {

    private int rpm;
    private boolean activo;

    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    public boolean isActivo() {
        return activo;
    }

    public void activa(){
        activo = true;
    }
    
    public void desactiva(){
        activo = false;
    }

    @Override
    public String toString() { 
        return "Motor:\n" + " RPM:" + rpm + "\n Encendido: " + (activo ? "SI":"NO") ;
     }
    
    
}
