/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrama;

/**
 *
 * @author Allan Murillo
 */
public class Coche {

    private Motor motor;
    private Persona conductor;

    public Coche(Motor motor) {
        this.motor = motor;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public String toString(Persona p) {
        return "Coche:\n " + motor + "\n Conductor:"
                + (!p.equals(conductor) ? conductor : "Yo ... xD");
    }

}
