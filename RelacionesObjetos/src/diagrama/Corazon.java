/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrama;

/**
 *
 * @author Allan Murillo
 */
public class Corazon {

    private int ritmo;

    public Corazon() {
    }

    public int getRitmo() {
        return ritmo;
    }

    public void setRitmo(int ritmo) {
        this.ritmo = ritmo;
    }

    @Override
    public String toString() {
        return "LPM: "+ ritmo; 
    }

}
