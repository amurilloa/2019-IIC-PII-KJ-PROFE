/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrama;

/**
 *
 * @author Allan Murillo
 */
public class Persona {
    
    private String nombre;
    private Corazon corazon;
    private Coche coche;
    
    public Persona(String nombre) {
        corazon = new Corazon();
        corazon.setRitmo(90);
        this.nombre = nombre;
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona\n " + nombre + "\n " + corazon + "\n" + (coche != null ? coche.toString(this):"Sin Carro");
    }
    
}