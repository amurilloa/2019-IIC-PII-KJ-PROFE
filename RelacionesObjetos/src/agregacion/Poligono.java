/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacion;

import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Poligono {

    private String color;
    private LinkedList<Segmento> segmentos;

    public Poligono(String color) {
        this.color = color;
        segmentos = new LinkedList<>();
    }

    public int cantSegm() {
        return segmentos.size();
    }

    public void agregarSegmento(Segmento s) {
        segmentos.add(s);
    }
}
