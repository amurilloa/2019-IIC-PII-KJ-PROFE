/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacion;

/**
 *
 * @author allanmual
 */
public class Segmento {

    private double dimension;

    public Segmento() {
    }

    public Segmento(double dimension) {
        this.dimension = dimension;
    }

    public double getDimension() {
        return dimension;
    }

    public void setDimension(double dimension) {
        this.dimension = dimension;
    }

    @Override
    public String toString() {
        return "D:" + dimension;
    }

}
