/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.perfiles;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Allan Murillo
 */
public class Util {

    public static ImageIcon cargarFoto(Foto f, int anchoLabel, int altoLabel) {
        ImageIcon icon = new ImageIcon(f.getRuta());
        //Mantener el aspecto
        int w = anchoLabel;
        int h = altoLabel;
        if (icon.getIconHeight() > icon.getIconWidth()) {
            double p = (double) icon.getIconHeight() / altoLabel;
            w = (int) (icon.getIconWidth() / p);
        } else {
            double p = (double) icon.getIconWidth() / anchoLabel;
            h = (int) (icon.getIconHeight() / p);
        }
        Image img = icon.getImage().getScaledInstance(w, h, Image.SCALE_SMOOTH);
        return new ImageIcon(img);
    }
}
