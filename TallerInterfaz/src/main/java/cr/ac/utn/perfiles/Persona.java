/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.perfiles;

import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author Allan Murillo
 */
public class Persona {

    private Foto foto;
    private String nombre;
    private String apellido;
    private final LinkedList<Lugar> lugares;
    private final LinkedList<Perfil> perfiles;

    public Persona() {
        lugares = new LinkedList<>();
        perfiles = new LinkedList<>();
    }

    public void agregarLugar(Lugar lugar) {
        lugares.add(lugar);
    }

    public void agregarPerfil(String perfil) {
        perfiles.add(new Perfil(perfiles.size() + 1, perfil));
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    public LinkedList<Lugar> getLugares() {
        return lugares;
    }

    public LinkedList<Perfil> getPerfiles() {
        return perfiles;
    }

    public Foto getFoto() {
        return foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nombre:\n").append(" ").append(nombre).append(" ").append(apellido);
        sb.append("\n\nPeriles:\n");
        for (Perfil perfil : perfiles) {
            sb.append(perfil.toString() + "\n");
        }
        sb.append("\nLugares:\n");
        for (Lugar lugar : lugares) {
            sb.append(lugar.toString() + "\n");
        }
        sb.append("\nFoto:\n").append(foto.getRuta());
        return sb.toString();
    }

    @Override
    public String toString() {
        return nombre + " " + apellido;
    }

}
