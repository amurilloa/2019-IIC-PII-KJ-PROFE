/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.perfiles;

/**
 *
 * @author Allan Murillo
 */
public class Lugar {

    private String lugar;
    private String direccion;
    private String telefono;

    public Lugar() {
    }

    public Lugar(String lugar, String direccion, String telefono) {
        this.lugar = lugar;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return lugar + " (" + (direccion.length() > 6 ? direccion.substring(0, 6) + "..." : direccion) + ")";
    }

}
