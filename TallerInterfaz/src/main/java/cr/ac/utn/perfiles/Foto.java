/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.perfiles;

/**
 *
 * @author Allan Murillo
 */
public class Foto {

    private String ruta;
    private float tamanno;
    private String tipo;

    public Foto() {
    }

    public Foto(String ruta, float tamanno, String tipo) {
        this.ruta = ruta;
        this.tamanno = tamanno;
        this.tipo = tipo;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public float getTamanno() {
        return tamanno;
    }

    public void setTamanno(float tamanno) {
        this.tamanno = tamanno;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Foto{" + "ruta=" + ruta + ", tamanno=" + tamanno + ", tipo=" + tipo + '}';
    }
}
