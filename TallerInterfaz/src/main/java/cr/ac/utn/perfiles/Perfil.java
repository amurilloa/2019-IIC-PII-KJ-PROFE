/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.perfiles;

/**
 *
 * @author Allan Murillo
 */
public class Perfil {

    private int tipo;
    private String perfil;

    public Perfil() {
    }

    public Perfil(int tipo, String perfil) {
        this.tipo = tipo;
        this.perfil = perfil;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    @Override
    public String toString() {
        return tipo + ". " + perfil;
    }

}
