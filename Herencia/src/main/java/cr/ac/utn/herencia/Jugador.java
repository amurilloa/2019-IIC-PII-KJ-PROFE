/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.herencia;

import java.time.LocalDate;

/**
 *
 * @author Allan Murillo
 */
public class Jugador extends Persona {

    private int dorsal;
    private String posicion;


    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    @Override
    public String toString() {
        String info = String.format("%s %s %d", super.toString(), posicion, dorsal);
        return info;
    }

    @Override
    public void concentrarse() {
        System.out.println("Concentrarse del Jugador");
    }

}
