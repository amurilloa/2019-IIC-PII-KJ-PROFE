/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.herencia;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Jugador p = new Jugador();
        p.setId(1);
        p.setNombre("Allan");
        p.setApellidos("Murillo Alfaro");
        p.setFechaNacimiento(LocalDate.parse("28/06/1988", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        p.setDorsal(1);
        p.setPosicion("POR");
        System.out.println(p);

        Jugador j = new Jugador();
        j.setId(2);
        j.setNombre("Lucas");
        j.setApellidos("Moura");
        j.setFechaNacimiento(LocalDate.parse("28/06/1988", DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        j.setDorsal(10);
        j.setPosicion("DEL");
        System.out.println(j);

        Masajista m = new Masajista();
        Entrenador e = new Entrenador();
        Persona[] personas = {m, j, e};
        for (Persona persona : personas) {
            if (persona instanceof Jugador) {
                System.out.println(((Jugador) persona).getDorsal());
            } else if (persona instanceof Persona) {
                System.out.println("Hola!!");
            } else {
                System.out.println("son de otro" + persona.getClass());
            }

        }
    }
}
