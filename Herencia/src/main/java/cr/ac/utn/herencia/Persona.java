/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.herencia;

import java.time.LocalDate;

/**
 *
 * @author Allan Murillo
 */
public abstract class Persona {

    protected int id;
    protected String nombre;
    protected String apellidos;
    protected LocalDate fechaNacimiento;

  
    
    public void viajar() {
        System.out.println("Viajaaaaaando....");
    }

    public abstract void concentrarse();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        String info = String.format("%d. %s %s (%d años)", id, nombre, apellidos, id);
        return info;
    }

}
