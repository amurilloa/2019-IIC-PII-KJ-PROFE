/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

import java.util.LinkedList;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private final LinkedList<Persona> personas;

    public Logica() {
        personas = new LinkedList<>();
        cargarEncuestas();
    }

    public void guardar(Persona p) {
        personas.add(p);
    }

    public int total() {
        return personas.size();
    }

    public float porMujeres() {
        int con = 0;
        for (Persona persona : personas) {
            if (persona.getGenero() == 'F') {
                con++;
            }
        }
        return con * 100f / personas.size();
    }

    public float porTrabajan() {
        int con = 0;
        for (Persona persona : personas) {
            if (persona instanceof Trabajador) {
                con++;
            }
        }
        return con * 100f / personas.size();
    }

    public float porMujeresT() {
        int con = 0;
        int per = 0;
        for (Persona persona : personas) {
            if (persona instanceof Trabajador) {
                per++;
                if (persona.getGenero() == 'F') {
                    con++;
                }
            }
        }
        return con * 100f / per;
    }

    private void cargarEncuestas() {
        for (int i = 0; i < 3; i++) {
            personas.add(new Trabajador(8, 0, 'M'));
            personas.add(new Trabajador(12, 1, 'F'));
            personas.add(new Trabajador(20, 2, 'F'));
            personas.add(new Estudiante('F'));
            personas.add(new Estudiante('F'));
            personas.add(new Estudiante('M'));
            personas.add(new Estudiante('M'));
            personas.add(new Estudiante('M'));
            personas.add(new Trabajador(40, 4, 'M'));
            personas.add(new Trabajador(24, 3, 'M'));
            personas.add(new Trabajador(8, 1, 'M'));
        }
        personas.add(new Trabajador(24, 3, 'M'));

        System.out.println(personas.size());
    }

    public DefaultPieDataset getDataJornada() {
        DefaultPieDataset ds = new DefaultPieDataset();
        int[] datos = new int[49];

//        for (int i = 0; i < personas.size(); i++) {
//            if (personas.get(i) instanceof Trabajador) {
//                int can = ((Trabajador) personas.get(i)).getHoras();
//                datos[can]++;
//            }
//        }
//
//        for (int i = 0; i < datos.length; i++) {
//            if (datos[i] > 0) {
//                ds.setValue(i == 1 ? "1hr" : (i + "hrs"), datos[i]);
//            }
//        }

        for (Persona persona : personas) {
            if (persona instanceof Trabajador) {
                int can = ((Trabajador) persona).getHoras();
                String key = can == 1 ? "1h" : (can + "hrs");
                if (ds.getIndex(key) > -1) {
                    ds.setValue(key, ds.getValue(key).intValue() + 1);
                } else {
                    ds.setValue(key, 1);
                }
            }
        }
        return ds;
    }

    public DefaultCategoryDataset getDataRangoSalarial() {
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        int s1 = 0;
        int s2 = 0;
        int s3 = 0;
        int s4 = 0;
        int s5 = 0;
        for (Persona persona : personas) {
            if (persona instanceof Trabajador) {
                if (((Trabajador) persona).getSalario() == 0) {
                    s1++;
                } else if (((Trabajador) persona).getSalario() == 1) {
                    s2++;
                } else if (((Trabajador) persona).getSalario() == 2) {
                    s3++;
                } else if (((Trabajador) persona).getSalario() == 3) {
                    s4++;
                } else if (((Trabajador) persona).getSalario() == 4) {
                    s5++;
                }
            }
        }

        ds.addValue(s1, "1", "-50");
        ds.addValue(s2, "1", "50-100");
        ds.addValue(s3, "1", "100-150");
        ds.addValue(s4, "1", "150-200");
        ds.addValue(s5, "1", "200+");
        return ds;
    }

}
