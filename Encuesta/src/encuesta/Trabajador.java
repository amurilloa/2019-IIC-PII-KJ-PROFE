/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author Allan Murillo
 */
public class Trabajador extends Persona {

    private int horas;
    private int salario;

    public Trabajador(int horas, int salario, char genero) {
        super(genero);
        this.horas = horas;
        this.salario = salario;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    /**
     * Nos retorna una descripción del rango salarial
     *
     * @return String descripción del rango salarial
     */
    public String getSalrioStr() {
        switch (salario) {
            case 0:
                return "menos de 50 mil";
            case 1:
                return "entre 50 y 100 mil";
            case 2:
                return "entre 100 y 150 mil";
            case 3:
                return "entre 150 y 200 mil";
            case 4:
                return "mas de 200";
            default:
                return "No indicado";
        }
    }

    @Override
    public String toString() {
        return String.format("%s | Horas: %d | Salario: %s ", super.toString(), horas,
                getSalrioStr());
    }

}
