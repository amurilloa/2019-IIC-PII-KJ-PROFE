/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author Allan Murillo
 */
public abstract class Persona {

    protected char genero;

    public Persona(char genero) {
        this.genero = genero;
    }

    /**
     * Asigna el valor del género, siempre convirtiendo el parámetro a mayúscula
     * @param genero char del genero a asignar
     */
    public void setGenero(char genero) {
        this.genero = Character.toUpperCase(genero);
    }

    public char getGenero() {
        return genero;
    }

    /**
     * Convierte el género de un char a una descipción tipo string
     * @return String descripción del genero
     */
    public String getGeneroStr() {
        return genero == 'M' ? "Masculino" : "Femenino";
    }

    @Override
    public String toString() {
        return "Género: " + getGeneroStr();
    }

}
