CREATE TABLE app.personas(
	id serial primary key,
	cedula text not null unique,
	nombre text not null,
	apellido_uno text not null, 
	apellido_dos text, 
	correo text not null unique
);

CREATE TABLE app.usuarios(
	usuario text not null unique, 
	contra text not null
) INHERITS (app.personas)
;
ALTER TABLE app.usuarios
    ADD CONSTRAINT usuarios_correo_unq UNIQUE (correo)
;

ALTER TABLE app.usuarios
    ADD CONSTRAINT usuarios_cedula_unq UNIQUE (cedula)
;
--drop table app.articulos
CREATE TABLE app.articulos(
  id serial primary key,
  codigo text not null unique,
  nombre text not null, 
  descripcion text not null,
  precio numeric default 0,
  estado boolean default true
)
;
CREATE TABLE app.fotos(
	cantidad integer
) INHERITS (app.articulos)
;
ALTER TABLE app.fotos
    ADD CONSTRAINT fotos_codigo_unq UNIQUE (codigo)
;

CREATE TABLE app.eventos(
	horas integer default 0
) INHERITS (app.articulos)
;
ALTER TABLE app.eventos
    ADD CONSTRAINT eventos_codigo_unq UNIQUE (codigo)
;

CREATE TABLE app.impresiones(
	id_tipo integer, 
	id_dimension integer
) INHERITS (app.articulos)
;

CREATE TABLE app.tipos(
	id serial primary key,
	tipo text not null unique,
	estado boolean default true 
)
;

drop table app.dimensiones
CREATE TABLE app.dimensiones(
	id serial primary key, 
	dimension text not null unique,
	ancho numeric default 0, 
	alto numeric default 0,
	id_unidad_medida integer, 
	estado boolean default true
)

CREATE TABLE app.unidades_medida(
	id serial primary key, 
	unidad text not null unique, 
	abreviatura text not null unique,
	estado boolean default true
)

ALTER TABLE app.impresiones
    ADD CONSTRAINT impresiones_codigo_unq UNIQUE (codigo)
;

ALTER TABLE app.dimensiones ADD CONSTRAINT fk_dim_unimed 
FOREIGN KEY (id_unidad_medida) REFERENCES app.unidades_medida(id);

ALTER TABLE app.impresiones ADD CONSTRAINT fk_imp_dim 
FOREIGN KEY (id_dimension) REFERENCES app.dimensiones(id);

ALTER TABLE app.impresiones ADD CONSTRAINT fk_imp_tip 
FOREIGN KEY (id_tipo) REFERENCES app.tipos(id);

insert into app.tipos(tipo) values 
('Papel Fotográfico'), 
('Acrílico'), 
('Tela');

insert into app.unidades_medida(unidad,abreviatura) values
('Pulgada', 'in'),
('Centímetro', 'cm'), 
('Metro','m');

select * from app.unidades_medida;

insert into app.dimensiones(dimension,alto,ancho,id_unidad_medida) values 
('Pasaporte',2,3,1), 
('Estándar',4,6,1), 
('5x7',5,7,1), 
('8x10',8,10,1)
;

-- select id, dimension, ancho, alto, id_unidad_medida, estado from app.dimensiones where id = ?;
-- select id, tipo, estado from app.tipos where id = ?
select id, unidad, abreviatura, estado from app.unidades_medida where id = ?
select * from app.impresiones

select * from app.articulos
delete from app.articulos where id = 2
insert into app.impresiones values(2,'IP01','Cuadro',	'Cuadro tamaño carta  en acrílico',	25000,true, 2,4)
--2	"IP01"	"Cuadro"	"Cuadro tamaño carta  en acrílico"	"25000"	true

update app.articulos  set estado = false where id = ?


ALTER TABLE app.usuarios ADD COLUMN tipo boolean NOT NULL DEFAULT false
SELECT * FROM app.usuarios


create table app.imagenes(
	id serial primary key, 
	img bytea not null
)

create table app.art_img(
	id serial primary key, 
	id_art int, 
	id_img int
)

ALTER TABLE app.art_img ADD CONSTRAINT fk_artimg_art 
FOREIGN KEY (id_art) REFERENCES app.articulos(id);


ALTER TABLE app.art_img ADD CONSTRAINT fk_artimg_img 
FOREIGN KEY (id_img) REFERENCES app.imagenes(id);











