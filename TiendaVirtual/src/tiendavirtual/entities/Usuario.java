/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

/**
 *
 * @author Allan Murillo
 */
public class Usuario extends Persona {
    
    private String usuario;
    private String contrasenna;
    private boolean tipo;

    public Usuario() {
    }

    public Usuario(String usuario, String contrasenna, boolean tipo, int id, String cedula, String nombre, String apellidoUno, String apellidoDos, String correo) {
        super(id, cedula, nombre, apellidoUno, apellidoDos, correo);
        this.usuario = usuario;
        this.contrasenna = contrasenna;
        this.tipo = tipo;
    }    

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }   

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public String toString() {
        return "Usuario{" + "usuario=" + usuario + ", contrasenna=" + contrasenna + '}';
    }
    
}
