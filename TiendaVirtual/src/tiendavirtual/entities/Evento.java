/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

/**
 *
 * @author Allan Murillo
 */
public class Evento extends Articulo {
    private int horas;

    public Evento() {
    }

    public Evento(int horas, int id, String codigo, String nombre, String descripcion, double precio, boolean estado) {
        super(id, codigo, nombre, descripcion, precio, estado);
        this.horas = horas;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    @Override
    public String toString() {
        return "Horas: " + horas ;
    }

}
