/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

import java.awt.Image;

/**
 *
 * @author Allan Murillo
 */
public class Imagen {

    private int id;
    private Image imagen;

    public Imagen() {
    }

    public Imagen(int id, Image imagen) {
        this.id = id;
        this.imagen = imagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Image getImagen() {
        return imagen;
    }

    public void setImagen(Image imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "Imagen " + Math.abs(id);
    }
}
