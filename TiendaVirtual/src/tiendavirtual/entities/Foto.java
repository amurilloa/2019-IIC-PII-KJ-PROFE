/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

/**
 *
 * @author Allan Murillo
 */
public class Foto extends Articulo {
    private int cantidad;

    public Foto() {
    }

    public Foto(int cantidad, int id, String codigo, String nombre, String descripcion, double precio, boolean estado) {
        super(id, codigo, nombre, descripcion, precio, estado);
        this.cantidad = cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Cantidad: " + cantidad;
    }
    
}
