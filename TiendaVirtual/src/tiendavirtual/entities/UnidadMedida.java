/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

/**
 *
 * @author Allan Murillo
 */
public class UnidadMedida {

    private int id;
    private String unidadMedida;
    private String abreviatura;
    private boolean estado;

    public UnidadMedida() {
    }

    public UnidadMedida(int id, String unidadMedida, String abreviatura, boolean estado) {
        this.id = id;
        this.unidadMedida = unidadMedida;
        this.abreviatura = abreviatura;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "UnidadMedida{" + "id=" + id + ", unidadMedida=" + unidadMedida + ", abreviatura=" + abreviatura + ", estado=" + estado + '}';
    }
}
