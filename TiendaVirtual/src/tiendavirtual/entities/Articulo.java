/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

import java.util.LinkedList;

/**
 *
 * @author Allan Murillo
 */
public abstract class Articulo {

    protected int id;
    protected String codigo;
    protected String nombre;
    protected String descripcion;
    protected double precio;
    protected boolean estado;
    protected LinkedList<Imagen> imagenes;

    public Articulo() {
        codigo = "";
        nombre = "";
        descripcion = "";
        imagenes = new LinkedList<>();
    }

    public Articulo(int id, String codigo, String nombre, String descripcion, double precio, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.estado = estado;
        imagenes = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public LinkedList<Imagen> getImagenes() {
        return imagenes;
    }

    public void setImagenes(LinkedList<Imagen> imagenes) {
        this.imagenes = imagenes;
    }

    @Override
    public String toString() {
        return "Articulo{" + "id=" + id + ", codigo=" + codigo + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio=" + precio + ", estado=" + estado + '}';
    }

}
