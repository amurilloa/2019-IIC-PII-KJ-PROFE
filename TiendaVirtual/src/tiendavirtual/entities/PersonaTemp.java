/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

/**
 *
 * @author Allan Murillo
 */
class PersonaTemp extends Object {

    private int cedula;
    private String nombre;
    private String apellido;

    public PersonaTemp() {
        this.cedula = 0;
    }

    public PersonaTemp(int cedula, String nombre, String apellido) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public static void main(String[] args) {
        PersonaTemp p = new PersonaTemp(206470762, "Allan", "Murillo");
        System.out.println(p);

    }

}
