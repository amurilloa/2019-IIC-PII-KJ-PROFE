/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

/**
 *
 * @author Allan Murillo
 */
public class Dimension {

    private int id;
    private String dimension;
    private int ancho;
    private int alto;
    private UnidadMedida unidad;
    private boolean estado;

    public Dimension() {
    }

    public Dimension(int id, String dimension, int ancho, int alto, UnidadMedida unidad, boolean estado) {
        this.id = id;
        this.dimension = dimension;
        this.ancho = ancho;
        this.alto = alto;
        this.unidad = unidad;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getAlto() {
        return alto;
    }

    public void setAlto(int alto) {
        this.alto = alto;
    }

    public UnidadMedida getUnidad() {
        return unidad;
    }

    public void setUnidad(UnidadMedida unidad) {
        this.unidad = unidad;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return dimension;
    }

}
