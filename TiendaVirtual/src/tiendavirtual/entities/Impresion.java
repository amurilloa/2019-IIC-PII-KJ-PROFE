/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.entities;

/**
 *
 * @author Allan Murillo
 */
public class Impresion extends Articulo {

    private Tipo tipo;
    private Dimension dimension;

    public Impresion() {
    }

    public Impresion(Tipo tipo, Dimension dimension) {
        this.tipo = tipo;
        this.dimension = dimension;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    @Override
    public String toString() {
        return "Tipo: " + tipo.getTipo() + ", Dimensión: " + getDimension().getDimension() + " " + getDimension().getUnidad().getAbreviatura();
    }

}
