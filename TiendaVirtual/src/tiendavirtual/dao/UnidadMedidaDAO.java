/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import tiendavirtual.entities.UnidadMedida;

/**
 *
 * @author Allan Murillo
 */
public class UnidadMedidaDAO {

    public UnidadMedida cargarID(int id) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, unidad, abreviatura, estado from"
                    + " app.unidades_medida where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return new UnidadMedida();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private UnidadMedida cargar(ResultSet rs) throws SQLException {
        UnidadMedida u = new UnidadMedida();
        u.setId(rs.getInt("id"));
        u.setUnidadMedida(rs.getString("unidad"));
        u.setAbreviatura(rs.getString("abreviatura"));
        u.setEstado(rs.getBoolean("estado"));
        return u;
    }

}
