/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import tiendavirtual.entities.Dimension;

/**
 *
 * @author Allan Murillo
 */
public class DimensionDAO {

    public Dimension cargarID(int id) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, dimension, ancho, alto, id_unidad_medida, "
                    + " estado from app.dimensiones where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return new Dimension();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public LinkedList<Dimension> cargar() {
        LinkedList<Dimension> dimensiones = new LinkedList<>();
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, dimension, ancho, alto, id_unidad_medida, "
                    + " estado from app.dimensiones where estado = true";
            PreparedStatement stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                dimensiones.add(cargar(rs));
            }
            return dimensiones;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private Dimension cargar(ResultSet rs) throws SQLException {
        Dimension d = new Dimension();
        d.setId(rs.getInt("id"));
        d.setDimension(rs.getString("dimension"));
        d.setAncho(rs.getInt("ancho"));
        d.setAlto(rs.getInt("alto"));
        d.setUnidad(new UnidadMedidaDAO().cargarID(rs.getInt("id_unidad_medida")));
        d.setEstado(rs.getBoolean("estado"));
        return d;
    }

}
