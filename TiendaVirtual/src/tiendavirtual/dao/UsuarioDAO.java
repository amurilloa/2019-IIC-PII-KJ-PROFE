/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import tiendavirtual.entities.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author Allan Murillo
 */
public class UsuarioDAO {

    public boolean insertar(Usuario usu) {
        try(Connection con = Conexion.getConexion()){
            String sql = "INSERT INTO app.usuarios(cedula, nombre, "
                    + "apellido_uno, apellido_dos, correo, usuario, contra) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usu.getCedula());
            stmt.setString(2, usu.getNombre());
            stmt.setString(3, usu.getApellidoUno());
            stmt.setString(4, usu.getApellidoDos());
            stmt.setString(5, usu.getCorreo());
            stmt.setString(6, usu.getUsuario());
            stmt.setString(7, usu.getContrasenna());
            
            return stmt.executeUpdate()>0;
        } catch(Exception e){
            throw new RuntimeException("Dato ya existe!!");
        }
    }

    public Usuario login(Usuario usu) {
         try(Connection con = Conexion.getConexion()){
             String sql = "SELECT id, cedula, nombre, "
                    + "apellido_uno, apellido_dos, correo, "
                     + "usuario, contra, tipo FROM app.usuarios "
                     + "WHERE contra = ? and (usuario = ? or correo = ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usu.getContrasenna());
            stmt.setString(2, usu.getUsuario());
            stmt.setString(3, usu.getCorreo());

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return cargar(rs);
            }
            return usu;
         }catch(Exception e){
             throw new RuntimeException("Problemas de conexión con el servidor");
         }
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt(1));
        u.setCedula(rs.getString(2));
        u.setNombre(rs.getString(3));
        u.setApellidoUno(rs.getString(4));
        u.setApellidoDos(rs.getString(5));
        u.setCorreo(rs.getString(6));
        u.setUsuario(rs.getString(7));
        u.setContrasenna(rs.getString(8));
        u.setTipo(rs.getBoolean(9));
        return u;
    }
    
}
