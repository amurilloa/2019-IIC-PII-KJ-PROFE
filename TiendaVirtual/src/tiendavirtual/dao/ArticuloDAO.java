/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import tiendavirtual.entities.Articulo;
import tiendavirtual.entities.Evento;
import tiendavirtual.entities.Foto;
import tiendavirtual.entities.Impresion;

/**
 *
 * @author Allan Murillo
 */
public class ArticuloDAO {

    public LinkedList<Articulo> cargar(String filtro, char estado) {
        LinkedList<Articulo> articulos = new LinkedList<>();
        filtro = generarFiltro(filtro, estado);

        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, nombre, descripcion, precio, estado, cantidad FROM app.fotos ";
            sql += filtro;
            PreparedStatement stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                articulos.add(cargarFoto(rs));
            }

            stmt.clearParameters();
            sql = "SELECT id, codigo, nombre, descripcion, precio, estado, horas FROM app.eventos ";
            sql += filtro;
            stmt = con.prepareStatement(sql);

            rs = stmt.executeQuery();
            while (rs.next()) {
                articulos.add(cargarEvento(rs));
            }

            stmt.clearParameters();
            sql = "SELECT id, codigo, nombre, descripcion, precio, estado, id_tipo, id_dimension FROM app.impresiones ";
            sql += filtro;
            stmt = con.prepareStatement(sql);

            rs = stmt.executeQuery();
            while (rs.next()) {
                articulos.add(cargarImpresion(rs));
            }
            return articulos;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private Impresion cargarImpresion(ResultSet rs) throws SQLException {
        Impresion i = new Impresion();
        i.setDimension(new DimensionDAO().cargarID(rs.getInt("id_dimension")));
        i.setTipo(new TipoDAO().cargarID(rs.getInt("id_tipo")));
        cargar(i, rs);
        return i;
    }

    private Evento cargarEvento(ResultSet rs) throws SQLException {
        Evento e = new Evento();
        e.setHoras(rs.getInt("horas"));
        cargar(e, rs);
        return e;
    }

    private Foto cargarFoto(ResultSet rs) throws SQLException {
        Foto f = new Foto();
        f.setCantidad(rs.getInt("cantidad"));
        cargar(f, rs);
        return f;
    }

    private void cargar(Articulo art, ResultSet rs) throws SQLException {
        art.setId(rs.getInt("id"));
        art.setCodigo(rs.getString("codigo"));
        art.setNombre(rs.getString("nombre"));
        art.setDescripcion(rs.getString("descripcion"));
        art.setPrecio(rs.getDouble("precio"));
        art.setEstado(rs.getBoolean("estado"));
    }

    public void activar(int id, boolean activo) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update app.articulos  set estado = ? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setBoolean(1, activo);
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (Exception e) {

            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public void actualiazar(Articulo art) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update app.%s set codigo=?, nombre=?, descripcion=?, precio=?, estado=?, %s where id = ?";
            PreparedStatement stmt = null;
            if (art instanceof Foto) {
                sql = String.format(sql, "fotos", "cantidad=?");
                stmt = con.prepareStatement(sql);
                stmt.setInt(6, ((Foto) art).getCantidad());
                stmt.setInt(7, art.getId());
            } else if (art instanceof Evento) {
                sql = String.format(sql, "eventos", "horas=?");
                stmt = con.prepareStatement(sql);
                stmt.setInt(6, ((Evento) art).getHoras());
                stmt.setInt(7, art.getId());
            } else if (art instanceof Impresion) {
                sql = String.format(sql, "impresiones", "id_tipo=?, id_dimension=?");
                stmt = con.prepareStatement(sql);
                stmt.setInt(6, ((Impresion) art).getTipo().getId());
                stmt.setInt(7, ((Impresion) art).getDimension().getId());
                stmt.setInt(8, art.getId());
            }
            stmt.setString(1, art.getCodigo());
            stmt.setString(2, art.getNombre());
            stmt.setString(3, art.getDescripcion());
            stmt.setDouble(4, art.getPrecio());
            stmt.setBoolean(5, art.isEstado());
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public void insertar(Articulo art) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO app.%s(codigo, nombre, descripcion, precio,"
                    + " estado, %s) VALUES (?, ?, ?, ?, ?, %s)";
            PreparedStatement stmt = null;
            if (art instanceof Foto) {
                sql = String.format(sql, "fotos", "cantidad", "?");
                stmt = con.prepareStatement(sql);
                stmt.setInt(6, ((Foto) art).getCantidad());
            } else if (art instanceof Evento) {
                sql = String.format(sql, "eventos", "horas", "?");
                stmt = con.prepareStatement(sql);
                stmt.setInt(6, ((Evento) art).getHoras());
            } else if (art instanceof Impresion) {
                sql = String.format(sql, "impresiones", " id_tipo, id_dimension", "?,?");
                stmt = con.prepareStatement(sql);
                stmt.setInt(6, ((Impresion) art).getTipo().getId());
                stmt.setInt(7, ((Impresion) art).getDimension().getId());
            }
            stmt.setString(1, art.getCodigo());
            stmt.setString(2, art.getNombre());
            stmt.setString(3, art.getDescripcion());
            stmt.setDouble(4, art.getPrecio());
            stmt.setBoolean(5, art.isEstado());
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private String generarFiltro(String filtro, char estado) {
        String txt = "";
        if (filtro.isBlank()) {
            if (estado != 'T') {
                txt = String.format(" where estado = %s", (estado == 'A' ? "true" : "false"));
            }
        } else if (!filtro.isBlank()) {
            txt = " where (lower(codigo) like lower('%s') or  lower(nombre) like lower('%s') or  lower(descripcion) like lower('%s'))";
            String dato = "%" + filtro.trim() + "%";
            txt = String.format(txt, dato, dato, dato);
            if (estado != 'T') {
                txt += String.format(" and (estado = %s)", (estado == 'A' ? "true" : "false"));
            }
        }
        return txt;
    }
}
