/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import tiendavirtual.entities.Tipo;

/**
 *
 * @author Allan Murillo
 */
public class TipoDAO {

    public Tipo cargarID(int id) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, tipo, estado from app.tipos where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargar(rs);
            }
            return new Tipo();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public LinkedList<Tipo> cargar() {
        LinkedList<Tipo> tipos = new LinkedList<>();
        try ( Connection con = Conexion.getConexion()) {
            String sql = "select id, tipo, estado from app.tipos where estado = true";
            PreparedStatement stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                tipos.add(cargar(rs));
            }
            return tipos;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private Tipo cargar(ResultSet rs) throws SQLException {
        Tipo t = new Tipo();
        t.setId(rs.getInt("id"));
        t.setTipo(rs.getString("tipo"));
        t.setEstado(rs.getBoolean("estado"));
        return t;
    }

}
