/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.bo;

import java.util.LinkedList;
import tiendavirtual.dao.TipoDAO;
import tiendavirtual.entities.Tipo;

/**
 *
 * @author Allan Murillo
 */
public class TipoBO {

    public LinkedList<Tipo> cargar() {
        return new TipoDAO().cargar();
    }
    
}
