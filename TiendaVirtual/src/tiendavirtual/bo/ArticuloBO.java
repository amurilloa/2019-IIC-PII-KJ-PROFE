/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.bo;

import java.util.LinkedList;
import tiendavirtual.dao.ArticuloDAO;
import tiendavirtual.entities.Articulo;

/**
 *
 * @author Allan Murillo
 */
public class ArticuloBO {

    public LinkedList<Articulo> cargarArticulos(String filtro, char estado) {
        return new ArticuloDAO().cargar(filtro.trim(), estado);
    }

    public void eliminar(int id) {
        if (id > 0) {
            new ArticuloDAO().activar(id, false);
        } else {
            throw new RuntimeException("Favor seleccione un artículo");
        }
    }
    
    public void activar(int id) {
        if (id > 0) {
            new ArticuloDAO().activar(id, true);
        } else {
            throw new RuntimeException("Favor seleccione un artículo");
        }
    }

    public void guardar(Articulo art) {
        // validaciones respectepivas del articulo 
        if(art.getId()> 0){
            new ArticuloDAO().actualiazar(art);
        } else {
            new ArticuloDAO().insertar(art);
        }
    }

}
