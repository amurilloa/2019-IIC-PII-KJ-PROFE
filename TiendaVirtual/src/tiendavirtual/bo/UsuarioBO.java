/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.bo;

import tiendavirtual.dao.UsuarioDAO;
import tiendavirtual.entities.Usuario;

/**
 *
 * @author Allan Murillo
 */
public class UsuarioBO {

    public boolean guardar(Usuario usu, String repass) {
        validar(usu, repass);
        usu.setContrasenna(Util.getSecurePassword(usu.getContrasenna()));
        return new UsuarioDAO().insertar(usu);
    }

    private void validar(Usuario usu, String repass) {
        if (usu == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (!usu.getContrasenna().equals(repass)) {
            throw new RuntimeException("Las contraseñas no coinciden");
        }

        if (usu.getContrasenna().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        if (usu.getCedula().trim().isEmpty()) {
            throw new RuntimeException("La cédula es requerida");
        }

        if (usu.getNombre().trim().isEmpty()) {
            throw new RuntimeException("El nombre es requerido");
        }

        if (usu.getApellidoUno().trim().isEmpty()) {
            throw new RuntimeException("El primer apellido es requerido");
        }

        if (usu.getCorreo().isBlank()) { //Versión 11 
            //TODO: Validar el formato del correo
            throw new RuntimeException("El correo es requerido");
        }

        if (usu.getUsuario().isBlank()) {
            throw new RuntimeException("El usuario es requerido");
        }
    }

    public Usuario login(Usuario usu) {
        if (usu == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (usu.getCorreo().isBlank() && usu.getUsuario().isBlank()) { //Versión 11 
            throw new RuntimeException("El correo/usuario requeridos");
        }

        if (usu.getContrasenna().trim().isEmpty()) {
            throw new RuntimeException("Contraseña debe digitar una contrase`ña");
        }
        usu.setContrasenna(Util.getSecurePassword(usu.getContrasenna()));
        return new UsuarioDAO().login(usu);
    }

}
