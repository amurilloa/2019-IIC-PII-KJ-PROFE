/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavirtual.bo;

import java.util.LinkedList;
import tiendavirtual.dao.DimensionDAO;
import tiendavirtual.entities.Dimension;

/**
 *
 * @author Allan Murillo
 */
public class DimensionBO {

    public LinkedList<Dimension> cargar() {
        return new DimensionDAO().cargar();
    }

}
